import java.util.Calendar;

public class Main {
    public static void main(String... args) {

//Method 1:
        BerlinClock berlinClock = new BerlinClock(BerlinClock.getCurrentTime());
        System.out.println("-----------------------------------------------------");
        System.out.println("** The current time is: " + BerlinClock.getCurrentTime() + "\n");
        System.out.println("-----------------------------------------------------");
        System.out.println("** The current time in Berlin Clock representation is: \n" );
        berlinClock.printTime();

//Method 2:
//        BerlinClock berlinClock = new BerlinClock("12:22:07");
//        System.out.println("** The time requested in Berlin Clock representation is: \n" );
//        berlinClock.printTime();

    }
}
