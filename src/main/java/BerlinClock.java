import java.util.Calendar;
import java.util.Collections;

public class BerlinClock {
    private String formattedTime;

    /**
     * Constructs a BerlinClock object
     * @param representedTime - the time that will be represented in the BC format given in HH:MM:SS format
     */
    public BerlinClock (String representedTime) {

        if(representedTime == null)
            throw new IllegalArgumentException("No time provided!");

        String[] time = representedTime.split(":", 3);

        int h = Integer.parseInt(time[0]);
        int m = Integer.parseInt(time[1]);
        int s = Integer.parseInt(time[2]);

        if (h<0 || h>23) throw new IllegalArgumentException("Hours out of range;");
        if (m<0 || m>59) throw new IllegalArgumentException("Minutes out of range");
        if (s<0 || s>59) throw new IllegalArgumentException("Seconds out of range");

        this.formattedTime = displayTime(h,m,s);

    }

    public static String getCurrentTime() {
        String currentTime;
        String h = Integer.toString(Calendar.getInstance().getTime().getHours());
        String m = Integer.toString(Calendar.getInstance().getTime().getMinutes());
        String s = Integer.toString(Calendar.getInstance().getTime().getSeconds());
        currentTime = h + ":" + m + ":" + s;
        return currentTime;
    }

    /** This method creates a String for each row of the clock.
     *
     * @param lampsOn - the lamps that are lighted
     * @param nrOfLamps - the nr of lamps on a row
     * @param lampType - the lamp type that ccan be Red (R), Yellow(Y) or Off (O)
     * @return A row of the clock as a String
     */
    private String clockRow (int lampsOn, int nrOfLamps, String lampType) {
        int lampsOff = nrOfLamps - lampsOn;
        String lightLamp = String.join("", Collections.nCopies(lampsOn,lampType));
        String offLamp = String.join("", Collections.nCopies(lampsOff,"O"));

        return lightLamp + offLamp;
    }

    /**
     * Method that converts hours minutes and seconds into the Berlin Clock representation
     *
     * @param h - hours as an int
     * @param m - minutes as an int
     * @param s - seconds as an int
     * @return Berlin Clock representation of the time;
     */
    private String displayTime(int h, int m, int s) {
        String row1 = (s % 2 == 0) ? "Y" + "\n" : "O" + "\n";
        String row2 = clockRow(h / 5, 4, "R") +"\n";
        String row3 = clockRow(h % 5, 4, "R")+ "\n";
        String row4 = clockRow(m / 5, 11, "Y")
                .replaceAll("YYY", "YYR") + "\n";
        String row5 = clockRow(m % 5, 4, "Y") + "\n";

        return row1 + row2 + row3 + row4 + row5;
    }

    /**
     *  Method that prints the hour in the Berlin Clock representation
     */
        public void printTime() {
            System.out.println(this);
        }


    @Override
    public String toString() {
        return this.formattedTime;
    }

}
